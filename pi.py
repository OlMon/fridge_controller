try:
    import RPi.GPIO as GPIO
except ImportError:
    class GPIO:

        HIGH = 1
        LOW = 0

        OUT = 0
        IN = 1

        BOARD = "BOARD"
        BCM = "BCM"
        
        @classmethod
        def setmode(cls, param):
            print("setmode to: {}".format(param))

        @classmethod
        def setup(cls, param1, param2, initial=LOW):
            print("setup pin {} to {}".format(param1, param2))

        @classmethod
        def output(cls, param1, param2):
            print("state pin {} set to {}".format(param1, param2))

        @classmethod
        def cleanup(cls):
            print("cleanup GPIO")

GPIO.setmode(GPIO.BCM)

def get_off():
    return GPIO.HIGH

def get_on():
    return GPIO.LOW

def pin_on(pin):
    GPIO.output(pin, GPIO.LOW)

def pin_off(pin):
    GPIO.output(pin, GPIO.HIGH)

def pin_make_out(pin, initial=GPIO.HIGH):
    GPIO.setup(pin, GPIO.OUT, initial=GPIO.HIGH)
