from sensor import Sensor
import random


class DummySensor(Sensor):

    name = "Dummy Sensor"

    def _read_data(self):
        return {"temperature": random.randint(10, 25),
                "humidity": random.randint(50, 100)}

    def __init__(self, idx, sensor_pins, db):
        super(DummySensor, self).__init__(idx, sensor_pins, db,
                                          ["temperature", "humidity"])

    # @property
    # def get_last_data_str(self):
    #     res = "Sensor[{}]".format(self.idx)
    #     for key, value in self.last_data.items():
    #         res += "\n\t{0}: {1}".format(key, value)
    #     return res
