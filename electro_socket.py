import threading
import pi

class ElectroSocket:
    """
    laskmd alskdmlaskmd alskdm

    alksmdal;ksmd alskmd
    """

    def __init__(self, name, pin):
        self.name = name
        self.pin = pin
        self.running_thread = None
        self.stop_event = None
        self.is_open = False
        self.open_sensor = 10
        self.close_sensor = 20
        self.open_time = 2
        self.close_time = 3
        self.first_close = False
        pi.pin_make_out(self.pin)
        self.close_switch()

    @property
    def first_open(self):
        return not self.first_close

    @first_open.setter
    def first_open(self, v):
        self.first_close = not v
        
    def open_switch(self):
        if not self.is_open:
            self.is_open = True
            pi.pin_on(self.pin)
            print(self.name, "open switch")

    def close_switch(self):
        if self.is_open:
            self.is_open = False
            pi.pin_off(self.pin)
            print(self.name, "close switch")

    @property
    def is_running(self):
        return self.running_thread is not None

    def inc_open_time(self):
        self.open_time += 1

    def dec_open_time(self):
        if self.open_time != 0:
            self.open_time -= 1

    def inc_close_time(self):
        self.close_time += 1

    def dec_close_time(self):
        if self.close_time != 0:
            self.close_time -= 1


    def inc_open_sensor(self):
        self.open_sensor += 1

    def dec_open_sensor(self):
        self.open_sensor -= 1

    def inc_close_sensor(self):
        self.close_sensor += 1

    def dec_close_sensor(self):
        self.close_sensor -= 1
    
    def _func_periodic(self, event,
                       open_time=3, close_time=5,
                       first_close=False):
        print(self.name, "starting Thread")
        self.close_switch()

        while True:
            if not first_close:
                self.open_switch()
                event.wait(timeout=open_time)
            first_close = False
            self.close_switch()
            event.wait(timeout=close_time)
            if event.is_set():
                break

        print(self.name, "ending Thread")
        self.close_switch()

    def _func_switch(self):
        if not self.is_open:
            self.open_switch()
        else:
            self.close_switch()

    def _func_sensor(self, event,
                     open, close,
                     sensor_f):
        print(self.name, "start Thread")

        while True:
            v = sensor_f()
            if open > close:
                if v > open:
                    self.open_switch()
                elif v <= close:
                    self.close_switch()
            elif open <= close:
                if v < open:
                    self.open_switch()
                elif v >= close:
                    self.close_switch()

            event.wait(timeout=3)
            if event.is_set():
                break

        print(self.name, "ending Thread")
        self.close_switch()

    def start_switch(self):
        self._func_switch()

    def start_periodic(self):
        self.stop_event = threading.Event()
        self.running_thread = threading.Thread(target=self._func_periodic,
                                               args=[self.stop_event,
                                                     self.open_time,
                                                     self.close_time,
                                                     self.first_close])
        self.running_thread.start()

    def start_sensor(self, sensor_f):
        self.stop_event = threading.Event()
        self.running_thread = threading.Thread(target=self._func_sensor,
                                               args=[self.stop_event,
                                                     self.open_sensor,
                                                     self.close_sensor,
                                                     sensor_f])
        self.running_thread.start()

    def stop(self):
        """Function to stop the `running_thread'."""
        if self.running_thread is not None:
            self.stop_event.set()
            self.running_thread.join()
            self.running_thread = None
