from sensor import Sensor
import time
import board
from adafruit_dht import DHT22
import pi

class DHT22Sensor(Sensor):

    name = "DHT22 Sensor"
    
    def _read_data(self):
        while True:
            try:
                temp = self._dht_device.temperature
                humi = self._dht_device.humidity
                if temp >= 150 or humi == 0:
                    continue
                return {"temperature": temp,
                        "humidity": humi}
            except RuntimeError as error:
            # Errors happen fairly often, DHT's are hard to read, just keep going
                if "DHT sensor not found, check wiring" == error.args[0]:
                    print("Sensor ID {} ".format(str(self.idx)) + "DHT DOWN")
                    if pi.GPIO.input(self.reset_pin) != pi.get_off():
                        pi.pin_off(self.reset_pin)
                        time.sleep(self.timeout + 0.5)
                        pi.pin_on(self.reset_pin)
                        print("Sensor ID {} ".format(str(self.idx)) + "DHT Restarted")
                    else:
                        print("Sensor ID {} ".format(str(self.idx)) + "DHT already Restarted. Normal waiting.")
                else:
                    print(error.args[0])
                time.sleep(self.timeout)
                continue
            except OverflowError:
                print("Overflow!")
                time.sleep(self.timeout)
                continue

            time.sleep(self.timeout)


    def __init__(self, idx, sensor_pin, reset_pin, db):
        super(DHT22Sensor, self).__init__(idx, sensor_pin, db,
                                          ["temperature", "humidity"])

        self.timeout = 2.5
        
        self.reset_pin = reset_pin
        pi.pin_make_out(self.reset_pin, initial=pi.get_on())

        if pi.GPIO.input(self.reset_pin) != pi.get_off():
            print("Sensor ID {} ".format(str(self.idx)) + "Init Reset")
            pi.pin_off(self.reset_pin)
            time.sleep(self.timeout)
            pi.pin_on(self.reset_pin)
        else:
            print("Sensor ID {} ".format(str(self.idx)) + "Init Reset only wait")
            time.sleep(self.timeout + 0.5)
            pi.pin_on(self.reset_pin)
        
        if self.sensor_pin == 11:
            self._dht_device = DHT22(board.D11, use_pulseio=True)
        elif self.sensor_pin == 9:
            self._dht_device = DHT22(board.D9, use_pulseio=True)
        elif self.sensor_pin == 10:
            self._dht_device = DHT22(board.D10, use_pulseio=True)
        elif self.sensor_pin == 22:
            self._dht_device = DHT22(board.D22, use_pulseio=True)
