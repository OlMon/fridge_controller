import json
from db import DB
from sensors_manager import SensorsManager
from socket_manager import SocketManager
from ui.settings_page import SettingsManager
import ui.ui_maker as ui

db = DB()

with open('settings.json') as f:
    d = json.load(f)
    sensor_manager = SensorsManager.get_sensors_from_json(d["sensors"], db)
    socket_manager = SocketManager.get_sockets_from_json(d["sockets"])

with open('uisettings.json') as f:
    d = json.load(f)
    settings_manager = SettingsManager(d["settings"])

ui.build_ui(socket_manager, sensor_manager, settings_manager, db)
ui.start_app()
