((nil . ((ol/compile-commands . ("python -m main"))
         (project-vc-ignores . "")

         (eglot-workspace-configuration
          . (:pylsp (:configurationSources ["pycodestyle"] ; string array: ["flake8"] or ["pycodestyle"] (default)
                                           :plugins (:autopep8
                                                     (:enabled :json-false) ; boolean: true (default) or false
                                                     :flake8
                                                     (:enabled :json-false) ; boolean: true or false (default)
                                                     :mccabe
                                                     (:enabled :json-false) ; boolean: true (default) or false
                                                     :pycodestyle
                                                     (:enabled :json-false ; boolean: true (default) or false
                                                               :convention "numpy"
                                                               :exclude [] ; string array: [] (default)
                                                               :filename [] ; string array: [] (default)
                                                               :hangClosing nil ; boolean: true or false; null (default)
                                                               :ignore [] ; string array: [] (default)
                                                               :indentSize nil ; integer: null (default)
                                                               :maxLineLength 88 ; integer: null (default)
                                                               :select nil) ; string array: null (default)
                                                     :pydocstyle
                                                     (:enabled :json-false
                                                               :addIgnore [] ; string array: [] (default)
                                                               :addSelect [] ; string array: [] (default)
                                                               :convention nil ; string: "None", "numpy" or "pep257"; null (default)
                                                               :enabled :json-false ; boolean: true or false (default)
                                                               :ignore [] ; string array: [] (default)
                                                               :match "(?!test_).*\\.py" ; string: "(?!test_).*\\.py" (default)
                                                               :matchDir "[^\\.].*" ; string: "[^\\.].*" (default)
                                                               :select nil) ; string array: null (default)
                                                     :pyflakes
                                                     (:enabled :json-false) ; boolean: true (default) or false
                                                     :pylint
                                                     (:enabled :json-false) ; boolean: true or false (default)
                                                     :pylsp_mypy
                                                     (:enabled t
                                                               :config_sub_paths [] ; string array: [] (default)
                                                               :dmypy :json-false ; boolean: true or false (default)
                                                               :dmypy_status_file "dmypy.json" ; string: "dmypy.json" (default)
                                                               :live_mode t ; boolean: true (default) or false
                                                               :overrides ["--ignore-missing-imports" t] ; string-array: [True] (default)
                                                               :report_progress :json-false ; boolean: true or false (default)
                                                               :strict :json-false) ; boolean: true or false (default)
                                                     :black
                                                     (:enabled t
                                                               :cache_config :json-false
                                                               :line_length 88
                                                               :preview t)
                                                     :ruff
                                                     (:enabled t
                                                               :formatEnable :json-false
                                                               :config nil
                                                               :exclude nil
                                                               :executable "ruff"
                                                               :ignore nil
                                                               :extendIgnore nil
                                                               :lineLength nil
                                                               :perFileIgnores nil
                                                               :select ["E" "F" "D"]
                                                               :extendSelect ["B" "Q"]
                                                               :preview t)
                                                     :rope_autoimport
                                                     (:enabled :json-false ; boolean: true or false (default)
                                                               :memory :json-false) ; boolean: true or false (default)
                                                     :rope_completion
                                                     (:eager :json-false ; boolean: true or false (default)
                                                             :enabled :json-false) ; boolean: true or false (default)
                                                     :rope_rename
                                                     (:enabled :json-false)
                                                     :pylsp_rope
                                                     (:enabled t
                                                               :rename t)
                                                     :yapf
                                                     (:enabled :json-false)
                                                     :rope
                                                     (:enabled t
                                                               :extensionModules nil ; string: null (default)
                                                               :ropeFolder nil
                                                               :rename t)) ; boolean: true (default) or false
                                           )))
         
         ))
(python-ts-mode . ((eval . (conda-env-activate "controller_env"))))
(prog-mode . ((eval . (eglot-ensure)))))
