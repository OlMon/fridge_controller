from abc import ABC, abstractmethod
import time
import threading


class Sensor(ABC):

    name = ""

    def __init__(self, idx, sensor_pin, db, data_types=[]):
        self.idx = idx
        self.db = db
        self.data_types = data_types
        self.sensor_pin = sensor_pin

        self.running_thread = None
        self.last_data = {}
        self.TIMEOUT = 2.5

        self.init_valuetypes_in_db()

    def init_valuetypes_in_db(self):
        self.db.init_valuetypes(self.data_types)

    @abstractmethod
    def _read_data(self):
        pass

    def _get_sensor_data(self, event):
        while True:
            read_time = int(time.time())
            self.last_data = self._read_data()
            data = []
            for key, value in self.last_data.items():
                data.append([self.idx, key, value, read_time])
            self.db.write_sensor_data(data)
            event.wait(timeout=self.TIMEOUT)
            if event.is_set():
                break

    def start_get_sensor_data(self):
        self.stop_event = threading.Event()
        self.running_thread = threading.Thread(target=self._get_sensor_data,
                                               args=[self.stop_event])
        self.running_thread.start()

    def get_last_data_by_type(self, type):
        return self.last_data.get(type)

    @property
    def get_last_data_str(self):
        res = self.get_gui_name()
        for key, value in self.last_data.items():
            res += "\n\t{0}: {1}".format(key, value)
        return res

    
    def get_last_data_dict(self):
        k = self.get_gui_name()
        d = {}
        d[k] = self.last_data
        return d

    def __del__(self):
        self.cleanup()

    def cleanup(self):
        if self.running_thread is not None:
            self.stop_event.set()
            self.running_thread.join()
            self.running_thread = None

    def get_gui_name(self):
        return "{0} (ID: {1})".format(type(self).name, self.idx)
