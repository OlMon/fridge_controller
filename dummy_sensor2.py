from sensor import Sensor
import random


class DummySensor2(Sensor):

    name = "Dummy Sensor 2"

    def _read_data(self):
        return {"Happiness": random.randint(10, 25),
                "Lust": random.randint(50, 100)}

    def __init__(self, idx, sensor_pins, db):
        super(DummySensor2, self).__init__(idx, sensor_pins, db,
                                          ["Happines", "Lust"])
    
    # @property
    # def get_last_data_str(self):
    #     res = "Sensor[{}]".format(self.idx)
    #     for key, value in self.last_data.items():
    #         res += "\n\t{0}: {1}".format(key, value)
    #     return res
