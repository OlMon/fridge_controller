from nicegui import ui
import os
import json

class SettingsManager():

    def __init__(self, settings_dict):     

        self.colormap = {"red": "#A11", "green": "#1A1", "gray": "#555"}
        self.settings_dict = settings_dict
        self.dark = ui.dark_mode()
        self.version = "0.5"


    def make_settings(self):
        with ui.column().classes("w-full"):
            with ui.card().classes("w-full"):
                with ui.column().classes("w-full"):
                    ui.label("Theme")
                    with ui.row().classes("w-full"):
                        # ui.button("Default", on_click=lambda: self.set_color("default"))
                        # ui.button("Gray", on_click=lambda: self.set_color("gray"))
                        # ui.button("Green", on_click=lambda: self.set_color("green"))
                        # ui.button("Red", on_click=lambda: self.set_color("red"))
                        self.color_toggle = ui.toggle({"default": 'Blue', "gray": 'Gray', "green": 'Green', "red" : "Red"},
                                                on_change=lambda: self.set_color(self.color_toggle.value),
                                                value=self.settings_dict["color"])
                        ui.space()
                        # ui.button("Dark", on_click=self.set_dark_mode)
                        # ui.button("Light", on_click=self.set_light_mode)
                        self.dark_toggle = ui.toggle({"light": "Light", "dark": "Dark"},
                                                     on_change=lambda: self.set_dark_light_mode(self.dark_toggle.value),
                                                     value="dark" if self.settings_dict.get("dark_mode") else "dark")
            with ui.card().classes("w-full"):
                with ui.column().classes("w-full"):
                    ui.label("Settings")
                    with ui.row().classes("w-full"):
                        ui.button("Restart App", on_click=lambda: os.utime("main.py"))
                        ui.space()
                        ui.switch("Dev Version")
                        ui.button("Update App", on_click=lambda: print("updating"))
            ui.button("Save", on_click=self.save_settings).bind_enabled_from(self, "settings_file_is_not_equal").classes("w-full")
            with ui.row().classes("w-full"):
                ui.space()
                ui.label("Fridge Controller, by OlMon")
                ui.label(f"Version: {self.version}")


    def set_color(self, color):
        ui.colors(primary=self.colormap.get(color))
        self.settings_dict["color"] = color


    def set_light_mode(self):
        self.dark.disable()
        self.settings_dict["dark_mode"] = False


    def set_dark_mode(self):
        self.dark.enable()
        self.settings_dict["dark_mode"] = True

    def set_dark_light_mode(self, mode):
        if mode == "light":
            self.set_light_mode()
        elif mode == "dark":
            self.set_dark_mode()
        

    def init_settings(self):
        if "color" in self.settings_dict:
            c = self.settings_dict["color"]
            ui.colors(primary=self.colormap.get(c))
        if "dark_mode" in self.settings_dict and not self.settings_dict.get("dark_mode"):
            self.dark.disable()
            self.settings_dict["dark_mode"] = False
        else:
            self.dark.enable()
            self.settings_dict["dark_mode"] = True

    @property
    def settings_file_is_not_equal(self):
        self.settings_manager = {}
        with open('uisettings.json') as f:
            d = json.load(f)
            settings_manager = d["settings"]
        return not settings_manager == self.settings_dict

    def save_settings(self):
        json_out = {}
        json_out["settings"] = self.settings_dict
        with open("uisettings.json", "w") as f:
            f.write(json.dumps(json_out, indent=4))
