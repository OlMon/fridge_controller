from nicegui import ui
from .socket_cards import make_socket_panel
from .graph import make_graph
#from .settings_page import make_settings, init_settings
from .log import make_log

import nicegui.binding
# Increase threshold for binding propagation warning from 0.01 to 0.02 seconds
nicegui.binding.MAX_PROPAGATION_TIME = 0.03

def make_header_tabs():
    with ui.header().classes(replace="row items-center") as header:
        with ui.tabs() as tabs:
            ui.tab("Sockets Left")
            ui.tab("Sockets Right")
            ui.tab("Graph")
            ui.tab("Log")
            ui.tab("Settings")

            return tabs, header


def populate_header_tabs(tabs, socket_manager, sensor_manager, settings_manager, db):
    with ui.tab_panels(tabs, value="Sockets Left").classes("w-full"):
        with ui.tab_panel("Sockets Left"):
            make_socket_panel(socket_manager.get("left"), sensor_manager)
        with ui.tab_panel("Sockets Right"):
            make_socket_panel(socket_manager.get("right"), sensor_manager)
        with ui.tab_panel("Graph"):
            make_graph(db, sensor_manager)
        with ui.tab_panel("Log"):
            make_log(sensor_manager, socket_manager)
        with ui.tab_panel("Settings"):
            settings_manager.make_settings()
            


def build_ui(socket_manager, sensor_manager, settings_manager, db):
    settings_manager.init_settings()
    tabs, header = make_header_tabs()
    populate_header_tabs(tabs, socket_manager, sensor_manager, settings_manager, db)


def start_app():
    # build_ui()
    ui.run(host="0.0.0.0", port=26666, reload=True)
