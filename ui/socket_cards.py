from nicegui import ui


def make_switch_panel(esocket):
    with ui.tab_panel('Switch'):
        switch_button = ui.button('ON', on_click=lambda: esocket.start_switch()).classes("w-full")
        switch_button.bind_text_from(esocket, 'is_open', backward=lambda n: "OFF" if n else "ON")


def make_periodic_panel(esocket):
    with ui.tab_panel('Periodic'):
        def toggle_socket(esocket):
            if esocket.is_running:
                esocket.stop()
            else:
                esocket.start_periodic()

        period_button = ui.button('Start Periodic', on_click=lambda: toggle_socket(esocket)).classes("w-full")
        period_button.bind_text_from(esocket, 'is_running', backward=lambda n: 'Stop Periodic' if n else 'Start Periodic')
        with ui.column().classes('w-full'):
            with ui.row().classes('w-full'):
                ui.checkbox("First Close", value=True).bind_value(esocket, "first_open").bind_text_from(
                    esocket,
                    "open_time",
                    backward=lambda n: ("First " if esocket.first_open else "Second ") + f'Open: {(n // 60):01d}:{(n % 60):02d}min'
                ).classes('w-full').bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)
            with ui.row().classes('w-full'):
                ui.button("-1M", on_click=lambda: [esocket.dec_open_time() for i in range(60)]).classes("w-2/12 flex-1").bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)
                ui.button("-1S", on_click=lambda: esocket.dec_open_time()).classes("w-2/12  flex-1").bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)
                ui.button("+1S", on_click=lambda: esocket.inc_open_time()).classes("w-2/12  flex-1").bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)
                ui.button("+1M", on_click=lambda: [esocket.inc_open_time() for i in range(60)]).classes("w-2/12  flex-1").bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)
            with ui.row().classes('w-full flex-1'):
                ui.checkbox("First Close").bind_value(esocket, "first_close").bind_text_from(
                    esocket,
                    "close_time",
                    backward=lambda n: ("First " if esocket.first_close else "Second ") + f'Close: {(n // 60):01d}:{(n % 60):02d}min'
                ).bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)
            with ui.row().classes('w-full'):
                ui.button("-1M", on_click=lambda: [esocket.dec_close_time() for i in range(60)]).classes("w-2/12 flex-1").bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)
                ui.button("-1S", on_click=lambda: esocket.dec_close_time()).classes("w-2/12 flex-1").bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)
                ui.button("+1S", on_click=lambda: esocket.inc_close_time()).classes("w-2/12 flex-1").bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)
                ui.button("+1M", on_click=lambda: [esocket.inc_close_time() for i in range(60)]).classes("w-2/12 flex-1").bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)


@ui.refreshable
def sensor_selection(d, esocket=None, sensor_manager=None):
    d["selected_sensor"], set_selected_sensor = ui.state(list(sensor_manager.get_available_sensor().keys())[0])
    d["selected_type"],  set_selected_type = ui.state(sensor_manager.get_available_sensor().get(d["selected_sensor"])[0])
    
    ui.select(list(sensor_manager.get_available_sensor().keys()),
              value=d["selected_sensor"],
              on_change=lambda s: [set_selected_sensor(s.value),
                                   set_selected_type(sensor_manager.get_available_sensor().get(s.value)[0])]).classes("w-full").bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)
    ui.radio(sensor_manager.get_available_sensor().get(d["selected_sensor"]),
             value=d["selected_type"],
             on_change=lambda t: set_selected_type(t.value)).props('inline').bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)

def make_sensor_panel(esocket, sensor_manager):
    with ui.tab_panel('Sensor'):
        def toggle_socket(esocket, f):
            if esocket.is_running:
                esocket.stop()
            else:
                esocket.start_sensor(f)

        d = {"selected_sensor": None, "selected_type": None}

        sensor_button = ui.button('Start Sensor',
                                  on_click=lambda: toggle_socket(esocket,
                                                                 sensor_manager.get_sensor_reader_by_name(d['selected_sensor'], d['selected_type']))
                                  ).classes("w-full") #TODO
        sensor_button.bind_text_from(esocket, 'is_running', backward=lambda n: 'Stop Sensor' if n else 'Start Sensor')

        with ui.column().classes("w-full"):
            sensor_selection(d, esocket, sensor_manager)
            
        with ui.column().classes("w-full"):
            with ui.row().classes("w-full"):
                ui.button("-5", on_click=lambda: [esocket.dec_open_sensor() for i in range(5)]).classes("flex-1").bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)
                ui.button("-", on_click=lambda: esocket.dec_open_sensor()).classes("flex-1").bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)
                ui.label().bind_text_from(esocket, "open_sensor", backward=lambda n: f'Open: {n:03d}').classes("w-4/16 flex-init")
                ui.button("+", on_click=lambda: esocket.inc_open_sensor()).classes("flex-1").bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)
                ui.button("+5", on_click=lambda: [esocket.inc_open_sensor() for i in range(5)]).classes("flex-1").bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)
            with ui.row().classes("w-full"):
                ui.button("-5", on_click=lambda: [esocket.dec_close_sensor() for i in range(5)]).classes("flex-1").bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)
                ui.button("-", on_click=lambda: esocket.dec_close_sensor()).classes("flex-1").bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)
                ui.label().bind_text_from(esocket, "close_sensor", backward=lambda n: f'Close: {n:03d}').classes("w-4/16 flex-init")
                ui.button("+", on_click=lambda: esocket.inc_close_sensor()).classes("flex-1").bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)
                ui.button("+5", on_click=lambda: [esocket.inc_close_sensor() for i in range(5)]).classes("flex-1").bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)


def make_card(esocket, sensor_manager):
    with ui.card().classes('h-full w-[calc(50%-2%)]'):
        with ui.tabs().classes("w-full") as inner_tabs:
            ui.tab('Switch').bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)
            ui.tab('Periodic').bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)
            ui.tab('Sensor').bind_enabled_from(esocket, 'is_running', backward=lambda n: not n)
        with ui.tab_panels(inner_tabs, value='Switch').classes('w-full h-72'):
            make_switch_panel(esocket)
            make_periodic_panel(esocket)
            make_sensor_panel(esocket, sensor_manager)
        ui.separator()
        with ui.row().classes("w-full justify-center"):
            ui.icon("check_circle_outline", color="#0F0").bind_visibility_from(esocket, 'is_open').classes('text-3xl')
            ui.icon("highlight_off", color="#F00").bind_visibility_from(esocket, 'is_open', backward=lambda n: not n).classes('text-3xl')



def make_socket_panel(sockets, sensor_manager):
    with ui.splitter(value=85).classes('w-full h-full') as splitter:
        splitter.disable()
        with splitter.after:
            with ui.tabs().props('vertical').classes('w-full') as tabs:
                top_row = ui.tab('Top Row', icon='expand_less')
                bottom_row = ui.tab('Bottom Row', icon='expand_more')
        with splitter.before:
            with ui.tab_panels(tabs, value=top_row).props('vertical').classes('w-full h-full'):
                with ui.tab_panel(top_row):
                    with ui.row().classes('w-full'):
                        s = sockets.get("top")
                        make_card(s[0], sensor_manager)
                        make_card(s[1], sensor_manager)
                with ui.tab_panel(bottom_row):
                    with ui.row().classes('w-full'):
                        s = sockets.get("bottom")
                        make_card(s[0], sensor_manager)
                        make_card(s[1], sensor_manager)
