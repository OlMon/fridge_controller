from nicegui import ui
import datetime

def make_graph(db, sensor_manager):
    d = {}
    func_update_graph = lambda: create_graph(db.get_last_minutes_sensor_data(sensor_manager.get_sensor_by_gui_name(d.get("selected_sensor")),
                                                                             d.get("selected_type"), period.value * interval.value), number_of_points.value, graph_container)
    with ui.row():
        sensor_selection(d, sensor_manager=sensor_manager)
        period = ui.number(label="Time", value=1, min=1).classes('w-24')
        interval = ui.select({60:'h', 1:"min"}, label='interval', value=1).classes('w-24')
        number_of_points = ui.select({9: '10', 24: '25', 49: '50', 99: '100', 0: 'all'}, label="Datapoints", value=49).classes('w-24')
        ui.button("Display", on_click=func_update_graph)
        line_updates = ui.timer(1, func_update_graph, active=False)
        line_checkbox = ui.checkbox('live').bind_value(line_updates, 'active')

        
    graph_container = ui.column().classes("w-full")    
    create_graph(db.get_last_minutes_sensor_data(sensor_manager.get_sensor_by_gui_name(d.get("selected_sensor")),
                                                 d.get("selected_type"), period.value * interval.value), number_of_points.value, graph_container)


@ui.refreshable
def sensor_selection(d, sensor_manager=None):
    d["selected_sensor"], set_selected_sensor = ui.state(list(sensor_manager.get_available_sensor().keys())[0])
    d["selected_type"],  set_selected_type = ui.state(sensor_manager.get_available_sensor().get(d["selected_sensor"])[0])
    
    ui.select(list(sensor_manager.get_available_sensor().keys()),
              value=d["selected_sensor"],
              on_change=lambda s: [set_selected_sensor(s.value),
                                   set_selected_type(sensor_manager.get_available_sensor().get(s.value)[0])])
    ui.radio(sensor_manager.get_available_sensor().get(d["selected_sensor"]),
             value=d["selected_type"],
             on_change=lambda t: set_selected_type(t.value)).props('inline')
    

def create_graph(data, points, container):
    container.clear()
    with container:
        if len(data) > 0:
            if points == 0 or (len(data)//points) == 0:
                points = len(data)
            data = data[0::len(data)//points]
            x = [datetime.datetime.fromtimestamp(i[1]).strftime('%H:%M:%S') for i in data]
            y = [round(i[0], 1) for i in data]
            fig = {'data': [{'type': 'scatter',
                             'x': x,
                             'y': y,
                             'name': 'data'},
                            {'type': 'line',
                             'x': x,
                             'y': [sum(y) / len(y)] * len(y),
                             'mode': 'lines',
                             'name': 'avg',
                             'line': {'color': '#7F7F7F'}}],
                   'layout': {'margin': {'l': 45, 'r': 15, 't': 0, 'b': 75},
                              'height': '415',
                              'showlegend': False},
                   'config': {'displaylogo': False}}
            ui.plotly(fig).classes('w-full')

