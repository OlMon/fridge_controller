from nicegui import ui


def make_sensor_cards(sensors_manager):
    for sensor in sensors_manager.sensors:
        with ui.card().tight():
            with ui.card_section().classes("w-full p-3"):
                ui.label(sensor.get_gui_name())
            ui.separator()
            with ui.card_section().classes("w-full"):
                with ui.grid(columns=2):
                    for sensor_data in sensor.data_types:
                        ui.label(sensor_data)
                        ui.label("").bind_text_from(sensor, "last_data", backward=lambda ld, sd=sensor_data: ld.get(sd))

def make_socket_cards(socket_data):
    for s in socket_data:
        with ui.card().tight():
            with ui.card_section().classes("w-full p-3"):
                ui.label(s.name)
            ui.separator()
            with ui.card_section().classes("w-full"):
                with ui.grid(columns=2):
                    ui.label("Running")
                    ui.icon("check_circle_outline", color="#0F0").bind_visibility_from(s, 'running_thread', lambda rt: rt is not None).classes('text-xl')
                    ui.icon("highlight_off", color="#F00").bind_visibility_from(s, 'running_thread', lambda rt: rt is None).classes('text-xl')
                    
                    ui.label("Open")
                    ui.icon("check_circle_outline", color="#0F0").bind_visibility_from(s, 'is_open').classes('text-xl')
                    ui.icon("highlight_off", color="#F00").bind_visibility_from(s, 'is_open', lambda n: not n).classes('text-xl')

def make_log(sensor_manager, socket_manager):
    with ui.splitter(value=50).classes('w-full') as splitter:
        splitter.disable()
        with splitter.before:
            with ui.grid(columns=2).classes("w-full p-4"):
                make_sensor_cards(sensor_manager)
        
        with splitter.after:
            with ui.tabs().classes('w-full') as tabs:
                left_side = ui.tab('Left Sockets', icon='keyboard_arrow_left').classes("w-full")
                right_side = ui.tab('Right Sockets', icon='keyboard_arrow_right').classes("w-full")
            with ui.tab_panels(tabs, value=left_side).classes('w-full'):
                with ui.tab_panel(left_side):
                    with ui.grid(columns=2).classes("w-full"):
                        make_socket_cards(socket_manager.get("left").get("top"))
                        make_socket_cards(socket_manager.get("left").get("bottom"))
                            
                with ui.tab_panel(right_side):
                    with ui.grid(columns=2).classes("w-full"):
                        make_socket_cards(socket_manager.get("right").get("top"))
                        make_socket_cards(socket_manager.get("right").get("bottom"))
