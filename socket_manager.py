from electro_socket import ElectroSocket


class SocketManager():

    def __init__(self, socket_dict):
        self.sockets = socket_dict
        self.init_sockets()

    def init_sockets(self):
        """Initializes sockets in OFF state."""
        pass

    @classmethod
    def get_sockets_from_json(cls, json_dict):
        """Create socket manager from json string."""
        sockets = json_dict
        # TODO
        print("Start")
        for lrk, lrv in json_dict.items():
            #print(lrv)
            for tbk, tbv in lrv.items():
               socket_list = []
               for s in tbv:
                   socket_list.append(
                       ElectroSocket("{}_{} {} {}".format(lrk, tbk, s["id"], s["pin"]), s["pin"])
                   )
               # print(lrk, lrv, tbk, tbv)
               # sockets[lrk][tbk] = []
               # sockets[lrk][tbk].append(ElectroSocket("{}_{} {}".format(lrk, tbk)))
               sockets[lrk][tbk] = socket_list

        
        return cls(sockets)


    def get(self, key):
        return self.sockets.get(key)
    

    def get_socket_list(self):
        """Return a list of all sockets.

        return List of Sockets
        """
        return []
