from dummy_sensor import DummySensor
from dummy_sensor2 import DummySensor2

class SensorsManager():

    def __init__(self, sensors):
        self.sensors = sensors
        for s in sensors:
            s.start_get_sensor_data()

    @classmethod
    def get_sensors_from_json(cls, json_str, db):
        sensors = []
        for sensor_json in json_str:
            sensor = None
            if sensor_json['type'] == "dummy":
                sensor = DummySensor(sensor_json['id'],
                                     sensor_json['pin'],
                                     db)
            if sensor_json['type'] == "dummy2":
                sensor = DummySensor2(sensor_json['id'],
                                      sensor_json['pin'],
                                      db)
            if sensor_json['type'] == "dht22":
                from dht22_sensor import DHT22Sensor
                sensor = DHT22Sensor(sensor_json['id'],
                                     sensor_json['pin'],
                                     sensor_json['reset_pin'],
                                     db)
            if sensor is not None:
                sensors.append(sensor)
        return cls(sensors)


    def get_sensor_reader_by_name(self, sensor_name, sensor_data):
        sensor_idx = self.get_sensor_by_gui_name(sensor_name)
        return self.get_sensor_reader(sensor_idx, sensor_data)
    
    def get_sensor_reader(self, sensor_idx, sensor_data):
        return lambda: self.sensors[sensor_idx].last_data.get(sensor_data)

    def __len__(self):
        return len(self.sensors)

    def get_sensor_values(self, idx):
        return self.sensors[idx].data_types

    def get_sensor_by_gui_name(self, gui_name):
        for s in self.sensors:
            if gui_name == s.get_gui_name():
                return s.idx
    
    def get_available_sensor(self):
        sensor_data = {}
        for s in self.sensors:
            sensor_data[s.get_gui_name()] = s.data_types
        
        return sensor_data

    @property
    def get_all_last_data_dict(self):
        d = {}
        for s in self.sensors:
            for key, value in s.get_last_data_dict().items():
                d[key] = value

        return d
