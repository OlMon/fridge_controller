import sqlite3 as sql
import time
import threading

class DB:

    def __init__(self, db_file="db.sql"):
        self.db_file = db_file
        self._db_con = None
        self.writer_thread = None
        self._init_db()

    def _init_db(self):
        with sql.connect(self.db_file) as con:
            cur = con.cursor()
            cur.execute("""
            CREATE TABLE IF NOT EXISTS sensordata
            (sensor_id INT,
            value_type,
            value,
            timestamp,
            FOREIGN KEY (value_type) REFERENCES valutye (id));
            """)

            cur.execute("""
            CREATE TABLE IF NOT EXISTS valuetype
            (value_type UNIQUE);
            """)
            con.commit()
            cur.close()

    def init_valuetypes(self, data):
        with sql.connect(self.db_file) as con:
            try:
                cur = con.cursor()

                data = [[x] for x in data]

                cur.executemany("INSERT INTO valuetype VALUES(?)", data)
            except sql.IntegrityError:
                cur.close()
            else:
                con.commit()
                cur.close()

    def write_sensor_data(self, data):
        with sql.connect(self.db_file) as con:
            cur = con.cursor()
            cur.executemany("INSERT INTO sensordata VALUES(?, (SELECT rowid FROM valuetype WHERE value_type = ?), ?, ?)", data)
            con.commit()
            cur.close()

    def get_latest_sensor_data(self,
                               sensor_id,
                               sensor_type):
        with sql.connect(self.db_file) as con:
            cur = con.cursor()
            res = cur.execute("SELECT * FROM sensordata ORDER BY timestamp DESC LIMIT 1;")


    def get_last_minutes_sensor_data(self,
                                   sensor_id,
                                   sensor_type,
                                   minutes=1):
        with sql.connect(self.db_file) as con:
            cur = con.cursor()
            res = cur.execute("""
            SELECT value, timestamp
            FROM sensordata
            WHERE sensor_id = ?
            AND value_type = (SELECT rowid FROM valuetype WHERE value_type = ?)
            AND timestamp > ?
            ORDER BY timestamp;""", (sensor_id, sensor_type, int(time.time())-(minutes*60)))
        return res.fetchall()
        

    def create_report(self):
        pass
